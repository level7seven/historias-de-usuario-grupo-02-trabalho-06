# Histórias de usuário - Grupo 02 - Trabalho 06

Este trabalho tem como objetivo ao aluno mostrar sua capacidade de identificar e descrever corretamente histórias de usuário.

## Organização das Issues

### Milestones

Para organizar as Issues "história" foi criado uma milestone com o nome do Épico.

Dentro da milestone do épico contém todas as issues relacionadas com a história 
de cada épico.

[Lista de todos os milestones](https://gitlab.com/joaoo45/historias-de-usuario-grupo-02-trabalho-06/milestones)

### Issues

Armazenam a história por si só, cada issue possui um ID que é relacionad a uma história

#### Labels

Usado para mostrar graficamente que são histórias

## Integrantes do grupo
*  Danilo de Sousa
*  João Antonio
*  João Pedro Mantovani
*  Priscila
*  Wellington

## Instruções do trabalho

O trabalho é em grupo (de acordo com a organização já definida) e as entregas incluem:

* Um conjunto de Histórias de Usuário derivadas a partir dos requisitos definidos no trabalho de especificação de requisitos
* Utilizar pelos menos seis (6) requisitos funcionais e quebrar em no mínimo 3 histórias;
* Identificar os requisitos utilizados no trabalho original (colocando o Id do requisito na descrição do épico)
* Identificar a qual épica as histórias estão associadas, linkando as issues dos épicos nas issues das histórias.
* Utilizar o template de Cartões de Histórias de Usuário disponibilizado na descrição da issue. 
* As histórias deverão ser entregues no gitlab.com, compartilhando o projeto com o professor (usuário @brenofranca ou e-mail brenofranca@gmail.com e professor Sindo).
* Cadastrar as histórias de usuário e representar as épicas, seja por meio de milestones ou labels.

